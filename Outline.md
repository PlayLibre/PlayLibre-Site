PlayLibre - ("Fan") 'Zine for all things Libre and Games - Board and Video / Board and Video Games!

Theme? (ie RPG theme? Yeah, I guess go for RPG for Febuary, since thats what I + we've been doing :P).

Order of Content (details below):

 * Editorial / Editors intro

 * What's happening this month? (might push to back? "Coming up Next Month"? )
 * Releases of the last month!
 * Reviews (if any)     <= might switch
 * Artwork showcase     <=  these two
 * (Hidden) Gems
 * (Libre) Classics
 * After Action report(s)

 * Outlook on next issue

What's up this Month?
=========================

Eg, dates of events - OnFOSSLAN, LibreJam (start and end), etc. Oh, if there were a LibreGaming VoiceChat/Meetup, that too maybe :P.

January's Releases! (ie "Last Month's Releases")
====================================================
(only substantial / real "releases", not just every point release under the sun...)


Roundup
--------

Followed by, for those deemed most worthy :P :

In greater Depth
------------------

(potentially also Reviews, in the section following I guess).

Reviews
=========

Self explanatory.


Artwork showcase
====================
(eg Miniatures, 3d Art, Board Game stuff/drawing/paintings, Pixel Art)

(PS - Sound"Art" and Procedural generation stuff can go here also - neat Levels too, I guess :P ?)

New and interesting stuff!
---------------------------

->

Followed by perhaps showcasing older nice stuff :P
------------------------------------------------------


(Hidden) Gems
===============
Games that are really neat + have been around for a while (and, unlike "Classics" are actively maintained!), but that not that many people might be familiar with. Mix of review and introduction (focus on intro).

-> Endless Sky

Classics:
===============
Alternate between Video and Board Games. Depending on someone wanting to write up an article of course :P.

Both a bit of history and a bit of introduction/description of the game(s).

 Board games (prelim) list
---------------------------------

* Go
* 4 Handed Chaturanga
* Standard playing cards
* Chess :P .

 Video games (prelim) list:
---------------------------------

(unordered)

* Abuse
*

After Action report(s)
=======================
(potentially "Battle Report"s!)

- focus on experience, I guess a playthrough / "Lets play" style report could go here to (eg me with Endless Sky). Our Libregaming Escapades fit well here as well I think :P.
--> ("You may have missed in January?" - notable stuff, eg all piling into thingy / The Mana World (though that was December :P))

Outlook on next issue
========================
(eg theme, call for articles, notable reviews or things that we plan on including).
